class TrackingsController < ApplicationController

  def create
    current_user.track_user(User.find(params[:id]))

    respond_to do |format|
      format.js
    end
  end

end
