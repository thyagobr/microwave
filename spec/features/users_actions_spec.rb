RSpec.describe "A User", type: :feature do

  context "when logged in" do
    before(:each) do
      @user = FactoryGirl.create(:user, email: "dev@helabs.com")
      @other_user = FactoryGirl.create(:user, email: "other_user@helabs.com")
      login_as(@user, scope: :user)
    end

    it "searches for other users", js: true do
      visit root_path

      find("#search_glass").click
      fill_in "search_bar", with: "other_user@helabs.com"
      click_button "search"

      expect(page).to have_content "search results for"
      expect(page).to have_content "other_user@helabs.com"
    end

    it "sees profile from other users" do
      @other_user.posts << FactoryGirl.build(:post, body: "hell yeah")

      visit users_path(@other_user)

      expect(page).to have_content "hell yeah"
    end

    it "follows other users", js: true do
      visit users_path(@other_user)
      click_link I18n.t("user.tracker.track")

      expect(page).to have_content I18n.t("user.tracker.untrack")
    end

    it "is alerted when is tracked" do
      @other_user.track_user(@user)

      visit root_path
      expect(page).to_not have_css "div.no-notifications"
    end

    it "can dismiss a notification", js: true do
      # this spec seems to be inconstant...
      # if it fails randomly, maybe we have to clean the notifications
      # beforehand
      Notification.destroy_all
      @other_user.track_user(@user)
      @other_user.posts.create(body: "just another user")
      visit root_path
      expect(@user.notify?).to be_truthy
      page.find(".dropdown-toggle").click
      page.find(".notification-box").click
      expect(@user.notify?).to be_falsy
    end

  end

end
