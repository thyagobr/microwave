class HomeController < ApplicationController

  def search
    # params[:q] seems very insecure
    @search_term = params[:q]
    @users = User.search(params[:q]).to_a
  end

  def check_new_posts
    posts_count = current_user.timeline_posts.count
    # sanitize session input
    current_posts_count = Integer(session[:posts_count]) or current_posts_count = 0
    render json: { "new_posts": (posts_count - current_posts_count) }
  end

  def profile
    @user = User.find(params[:id])
  end

  def personal
    dimsissal = current_user.dismiss_notification(params[:notif_id])
    render nothing: true
  end

  def track_user
    current_user.track_user(User.find(params[:id]))
  end

end
