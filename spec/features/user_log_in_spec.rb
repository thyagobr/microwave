RSpec.describe "User logging in", type: :feature do

  let(:user) { FactoryGirl.create(:user) }
  
  it "redirects to log in screen when logged out" do
    visit root_path
    expect(page).to have_content I18n.t("devise.failure.unauthenticated")
  end

  it "user registers and is redirected to home#index" do
    sign_up # defined in: support/helpers/session_helpers.rb
    expect(page.current_path).to eq root_path
  end

  it "can edit its account" do
    login_as(user, scope: :user)
    visit edit_user_registration_path
    fill_in "user_name", with: "different name"
    fill_in "user_current_password", with: "codeyourboat" # password fixed on factory
    click_button "Update"
    expect(page).to have_content "Your account has been updated successfully."
  end
end
