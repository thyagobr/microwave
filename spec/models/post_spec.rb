RSpec.describe Post do
  context "creating a Post" do
    it "requires mandatory fields" do
      post = Post.new
      post.valid?
      expect(post.errors.keys).to include :body
      expect(post.errors.keys).to include :user
    end
  end
end
