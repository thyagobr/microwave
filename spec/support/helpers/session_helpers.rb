module SessionHelpers

  def sign_up(email: "dev@helabs.com",
              name: "mr. anderson",
              password: "codeyourboat",
              password_confirmation: "codeyourboat")
    visit new_user_registration_path
    fill_in "Name", with: name
    fill_in "Email", with: email 
    fill_in "Password", with: password
    fill_in "Password confirmation", with: password_confirmation
    click_button "Sign up"
  end

end
