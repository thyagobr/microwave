FactoryGirl.define do
  factory :user do
    email Faker::Internet.email
    name Faker::Name.name
    password "codeyourboat"
    password_confirmation "codeyourboat"    

    trait :with_posts do
      after(:build) do |instance|
        3.times { instance.posts << FactoryGirl.build(:post) }
      end
    end
  end

end
