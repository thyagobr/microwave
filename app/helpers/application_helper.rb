module ApplicationHelper
  #def flash_message
  #  messages = ""
  #  [:notice, :info, :warning, :error].each {|type|
  #    if flash[type]
  #      messages += "<p class=\"#{type}\">#{flash[type]}</p>"
  #    end
  #  }

  #  messages
  #end

  def current_user_in(users)
    users == users.reject! { |user| user == current_user }
  end

end
