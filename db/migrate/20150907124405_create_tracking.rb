class CreateTracking < ActiveRecord::Migration

  def change
    create_table :trackings do |t|
      t.integer :user_id
      t.integer :frequency_id
      t.timestamps null: false
    end

    add_index :trackings, :user_id
    add_index :trackings, :frequency_id
  end

end
