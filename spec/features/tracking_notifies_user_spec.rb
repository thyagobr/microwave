RSpec.describe Notification do

  let(:user) { FactoryGirl.create(:user) }
  let(:other_user) { FactoryGirl.create(:user, email: Faker::Internet.email) }

  context "a valid User" do

    it "is notified when tracked by someone else" do
      expect { other_user.track_user(user) }.to change { user.notifications.size }.by 1
    end

    it "knows who sent an invitation" do
      other_user.track_user(user)
      expect(user.notifications.first.notified_by).to eql other_user
    end

  end
end
