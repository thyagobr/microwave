RSpec.describe "Logging in with Github", type: :feature do

  context "when a visitor comes to the app" do
    it "signs up with github" do
      VCR.use_cassette "github_signup", record: :all do
        visit root_path
        expect { click_link "Sign in with Github" }.to change { User.count }.by(1)
      end
    end
  end

end
