RSpec.describe "Posts", type: :feature do

  # a logged in user can
  # create posts
  context "a logged in user" do
    let(:user) { FactoryGirl.create(:user) }

    before :each do
      login_as(user, scope: :user)
    end

    it "creates posts" do
      visit new_post_path

      fill_in :post_body, with: Faker::Lorem.paragraph

      expect { click_button "Microwave it!" }.to change { Post.count }.by 1
    end

    it "is redirected to the timeline on a failed post" do
      visit new_post_path
      click_button "Microwave it!"
      expect(page.current_path).to eq posts_path
    end

    it "sees a list of his own postings" do
      visit new_post_path
      fill_in :post_body, with: "Welcome to the Matrix =P"
      click_button "Microwave it!"

      visit posts_path
      expect(page).to have_content "Welcome to the Matrix =P"
    end

    it "sees his friends' posts on his timeline" do
      other_user = FactoryGirl.create(:user, email: "other_user@somewhere.com") 
      other_user.posts.create(body: "other_user's post")
      user.track_user(other_user)

      visit posts_path
      expect(page).to have_content("other_user's post")
    end

    it "sees notification of new posts when tracked frequencies post", js: true do
      other_user = FactoryGirl.create(:user, email: "other_user@somewhere.com") 
      user.track_user(other_user)
      visit posts_path
      other_user.posts.create(body: "new post avaliable")
      # the javascript kicks in in 3 seconds
      sleep 4
      expect(page).to have_content("1 new transmissions")
    end

  end
end
