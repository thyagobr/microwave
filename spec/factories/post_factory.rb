FactoryGirl.define do
  factory :post do
    user nil
    body Faker::Lorem.paragraph
  end
end
