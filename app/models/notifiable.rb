module Notifiable

  def notify(args = {})
    user = args[:user]
    return unless user
    family = args[:family] || "none"
    user.notifications.create(notified_by: self, family: family, dismissed: false)
  end
end
