class CreatePost < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.references :user, index: true, foreign_key: true, null: false
      t.integer :author # the :user is for ownership; this is for authorship
      t.text :body
      t.boolean :deleted
      t.timestamps null: false
    end
  end
end
