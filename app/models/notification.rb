class Notification < ActiveRecord::Base
  belongs_to :user
  belongs_to :notified_by, class_name: "User", foreign_key: "notified_by"
end
