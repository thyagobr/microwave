module OmniAuthHelpers

  GITHUB_OAUTH_HASH = { 'provider' => 'github', 
                        'uid' => '12345', 
                        'info' => {
                          'name' => 'Natasha Kramolinsk',
                          'email' => 'natasha@souldev.com',
                          'nickname' => 'natalinsk'
                        }
  }

end
