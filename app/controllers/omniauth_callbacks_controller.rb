class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def github
    @user = User.find_or_create_from_oauth(request.env["omniauth.auth"])
    flash[:notice] = "Hi, #{@user.email}!"
    sign_in_and_redirect @user
  end
end
