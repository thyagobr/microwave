Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: "omniauth_callbacks", registrations: "registrations" }

  root "posts#index"

  resources :posts

  get "/search", to: "home#search", as: "search"
  get "/check_new_posts", to: "home#check_new_posts", as: "check_new_posts"

  post "/users/:id/track", to: "home#track_user", as: "track_frequency"
  get "/users/:id", to: "home#profile", as: "users"

  post "/personal", to: "home#personal"

end
