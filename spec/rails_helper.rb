ENV['RAILS_ENV'] ||= 'test'
require 'spec_helper'
require File.expand_path('../../config/environment', __FILE__)
require 'rspec/rails'
require 'database_cleaner'
require 'capybara/rails'
require 'capybara/rspec'

Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

ActiveRecord::Migration.maintain_test_schema!

require 'omniauth'
OmniAuth.config.test_mode = true
OmniAuth.config.add_mock(:github, OmniAuthHelpers::GITHUB_OAUTH_HASH)

RSpec.configure do |config|
  config.infer_spec_type_from_file_location!
  config.render_views
  config.include Capybara::DSL, :type => :feature
  config.include FactoryGirl::Syntax::Methods
  config.include SessionHelpers, :type => :feature
  config.include OmniAuthHelpers
  config.include Warden::Test::Helpers
  config.before :suite do
    Warden.test_mode!
  end
  config.include ActiveSupport::Testing::TimeHelpers

  config.use_transactional_fixtures = false

  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do |example|
    DatabaseCleaner.strategy = example.metadata[:js] ? :truncation : :transaction
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

end
