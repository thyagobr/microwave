class PostsController < ApplicationController

  def index
    @post = current_user.posts.build
    @posts = current_user.timeline_posts.page params[:page]
    session[:posts_count] = @posts.size
  end

  def new
    @post = current_user.posts.build
  end

  def create
    post = current_user.posts.build(post_params)
    if post.save
      redirect_to post, notice: "Your new message is hot off the ...oven." 
    else
      redirect_to posts_path
    end
  end

  def show
    @post = current_user.posts.find(params[:id])
  end

  private

  def post_params
    params.require(:post).permit(:body)
  end

end
