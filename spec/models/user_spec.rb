require 'rails_helper'

RSpec.describe User, type: :model do

  context "finding or creating a User with OAuth" do
    it "creates from oauth" do
      expect do
        User.find_or_create_from_oauth OmniAuthHelpers::GITHUB_OAUTH_HASH
      end.to change { User.count }.by 1

      expect do
        User.find_or_create_from_oauth OmniAuthHelpers::GITHUB_OAUTH_HASH
      end.to_not change { User.count }
    end
  end

  context "a valid User" do

    let (:user) { FactoryGirl.create(:user, email: "dev@helabs.com") }
    let (:other_user) { FactoryGirl.create(:user, email: "other_user@helabs.com") }

    it "has many posts" do
      expect(user.posts.build).to be_an_instance_of Post
    end

    it "can track other users" do
      expect { user.track_user(other_user) }.to change { user.trackings.count }.by 1
    end

    it "knows he's being tracked by other users" do
      expect { other_user.track_user(user) }.to change { user.tracked_by.count }.by 1
    end

    it "can #dismiss_notification" do
      other_user.track_user(user)
      expect(user.notify?).to be_truthy
      user.dismiss_notification(user.notifications.first.id)
      expect(user.notify?).to be_falsy
    end

  end
end
