class User < ActiveRecord::Base
  include Notifiable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :omniauthable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :posts
  
  # make the tracking and infer tracked_by with a where clause
  has_many :trackings
  has_many :frequencies, through: :trackings
  has_many :notifications

  validates :name, presence: true, length: { in: 1..200 }

  def top_notifications
    notifications.order(created_at: "desc").limit(5)
  end

  def notify?
    top_notifications.any? { |n| n.dismissed == false }
  end

  def dismiss_notification(notification_id)
    notification = notifications.find(notification_id)
    if notification
      notification.update(dismissed: true, dismissed_at: DateTime.now)
    end
  end

  def track_user(user)
    if tracking = trackings.create(frequency: user) 
      notify(user: user, family: "tracking")
    end
  end

  def tracked_by
   Tracking.where(frequency_id: self.id)
  end
  
  def tracking?(user)
    trackings.where(frequency_id: user.id).size > 0
  end

  # fetches his/er own posts and the posts of users whose frequency s/he is tracking
  def timeline_posts
    ids = self.trackings.collect(&:frequency_id)
    ids << self.id
    Post.where("user_id IN (#{ids.join(',')})").order(created_at: "desc")
  end

  # used for displaying on views
  def to_s
    email
  end

  def self.find_or_create_from_oauth(oauth_env)
    User.where(email: oauth_env["info"]["email"]).first_or_create do |user|
      user.name = oauth_env["info"]["name"]
      user.email = oauth_env["info"]["email"]
      user.password = Devise.friendly_token
    end
  end

  def self.search(term)
    where("email ILIKE ?", "%#{term}%")
  end

end

