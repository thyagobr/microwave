class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :notified_by, index: true
      t.boolean :dismissed, default: false, index: true
      t.datetime :dismissed_at
      t.string :family, index: true
      t.string :message
      t.timestamps null: false
    end
  end
end
